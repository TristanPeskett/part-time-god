﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionCone : MonoBehaviour
{

    //[SerializeField]
    //private float radius = 3;
    //[SerializeField]
    //private float maxDistance = 3;
    [SerializeField]
    private float visionAngle = 30;

    private SphereCollider visionSphere;

    // Start is called before the first frame update
    void Start()
    {
        visionSphere = GetComponent<SphereCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        

    }

    private void OnTriggerStay(Collider other)
    {
        //If the object isn't draggable, stop
        if (!other.gameObject.GetComponent<DragRigidbody>())
            return;

        //If the object is floating
        if (other.gameObject.GetComponent<DragRigidbody>().floating)
        {
            //Determine vision angle, if within range get caught
            Vector3 targetDir = other.gameObject.transform.position - transform.position;
            float angle = Vector3.Angle(targetDir, transform.forward);
            if (angle < visionAngle)
            {
                //print("gotcha");
                transform.parent.GetComponent<GlobalCustomerStorage>().CaughtPlayerUsingAbility = true;
            }
                
                
            //Debug.Log(angle);

            //Loss

        }
    }

}
