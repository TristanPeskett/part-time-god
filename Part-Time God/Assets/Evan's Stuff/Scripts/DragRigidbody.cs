﻿using UnityEngine;

/// <summary>
/// Drag a rigidbody with the mouse using a spring joint.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class DragRigidbody : MonoBehaviour
{
    public float force = 600;
    public float damping = 60;

    Transform jointTrans;
    float dragDepth;
    float depth = 0;
    //float dragDepthAdjust = 0;
    [SerializeField]
    [Range(0.0f, 2.0f)]
    float dragDepthAdjustScale = 0.5f;
    [SerializeField]
    [Range(0.0f, 10.0f)]
    float depthAdjustBoostScale = 2f;

    public bool floating = false;


    private Camera playerCam;
    private Vector3 hitPoint = Vector3.zero;

    private void Start()
    {
        GameObject player = GameObject.Find("Player");
        playerCam = player.transform.Find("Main Camera").GetComponent<Camera>();
    }

    void OnMouseDown()
    {
        HandleInputBegin(Input.mousePosition);
    }

    void OnMouseUp()
    {
        HandleInputEnd(Input.mousePosition);
    }

    void OnMouseDrag()
    {
        HandleInput(Input.mousePosition);
    }

    public void HandleInputBegin(Vector3 screenPosition)
    {
        var ray = Camera.main.ScreenPointToRay(screenPosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Interactive"))
            {
                dragDepth = CameraPlane.CameraToPointDepth(playerCam, hit.point);
                jointTrans = AttachJoint(hit.rigidbody, hit.point);

                depth = Vector3.Distance(playerCam.transform.position, hit.point);

                //Player using powers
                FirstPersonMovement.usingDragging = true;
                floating = true;

                AnalyticsPing.itemsGrabbed++;

                hitPoint = hit.point;
            }
        }
    }

    public void HandleInput(Vector3 screenPosition)
    {
        if (jointTrans == null)
            return;

        dragDepth = CameraPlane.CameraToPointDepth(playerCam, jointTrans.position);

        var worldPos = playerCam.ScreenToWorldPoint(screenPosition);

        if (Input.GetKey(KeyCode.LeftShift))
        {
            //print("yeehaw");
            dragDepth += Input.mouseScrollDelta.y * (dragDepthAdjustScale * depthAdjustBoostScale);
            depth += Input.mouseScrollDelta.y * (dragDepthAdjustScale * depthAdjustBoostScale);
        } else
        {
            //print("wtf");
            dragDepth += Input.mouseScrollDelta.y * dragDepthAdjustScale;
            depth += Input.mouseScrollDelta.y * dragDepthAdjustScale;
        }

        //jointTrans.position = CameraPlane.ScreenToWorldPlanePoint(playerCam, dragDepth += Input.mouseScrollDelta.y * dragDepthAdjustScale, screenPosition);
        jointTrans.position = playerCam.transform.forward * depth + playerCam.transform.position;
        
        
    }

    public void HandleInputEnd(Vector3 screenPosition)
    {
        //Player done using powers
        FirstPersonMovement.usingDragging = false;
        floating = false;

        hitPoint = Vector3.zero;

        Destroy(jointTrans.gameObject);
    }

    Transform AttachJoint(Rigidbody rb, Vector3 attachmentPosition)
    {
        GameObject go = new GameObject("Attachment Point");
        go.hideFlags = HideFlags.HideInHierarchy;
        go.transform.position = attachmentPosition;

        var newRb = go.AddComponent<Rigidbody>();
        newRb.isKinematic = true;

        var joint = go.AddComponent<ConfigurableJoint>();
        joint.connectedBody = rb;
        joint.configuredInWorldSpace = true;
        joint.xDrive = NewJointDrive(force, damping);
        joint.yDrive = NewJointDrive(force, damping);
        joint.zDrive = NewJointDrive(force, damping);
        joint.slerpDrive = NewJointDrive(force, damping);
        joint.rotationDriveMode = RotationDriveMode.Slerp;

        return go.transform;
    }

    private JointDrive NewJointDrive(float force, float damping)
    {
        JointDrive drive = new JointDrive();
        drive.mode = JointDriveMode.Position;
        drive.positionSpring = force;
        drive.positionDamper = damping;
        drive.maximumForce = Mathf.Infinity;
        return drive;
    }
}