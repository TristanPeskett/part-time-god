﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;

public class DialogueSystem : MonoBehaviour
{
    //[SerializeField]
    //private GameObject textObject;
    [SerializeField]
    private Text textField;
    private StudioEventEmitter audioEmitter;
    private GlobalCustomerStorage storageScript;
    private bool shouldSpeak = false;

    private float charTimer = 0;
    [SerializeField]
    private float charDelay = 0.1f;
    private float lineTimer = 0;
    [SerializeField]
    private float lineDelay = 2.0f;
    private bool lineFinished = false;

    private string currentDisplay = "";
    private int progressTracker = 0;

    public string[] lines;
    private int currentLine = 0;
    private string selectedLine;


    void Start()
    {
        //audioEmitter = GetComponent<StudioEventEmitter>();
        
        storageScript = GetComponent<GlobalCustomerStorage>();
        selectedLine = lines[currentLine];
    }

    void Update()
    {
        

        if (shouldSpeak && !lineFinished && charTimer < Time.time)
        {
            selectedLine = lines[currentLine];

            //Next letter
            char currentLetter = selectedLine[progressTracker];
            currentDisplay += currentLetter;
            textField.text = currentDisplay;
            progressTracker++;

            //Play sound
            string cL = currentLetter.ToString();
            if (cL != " " && cL != "," && cL != "." && cL != "!" && cL != "\"" && cL != ":")
            {
                //audioEmitter.Play();
            }

            //Delay before next char
            if (cL == ",")
            {
                charTimer = Time.time + charDelay * 2;
            }
            else if (cL == "." || cL == ":")
            {
                charTimer = Time.time + charDelay * 4;
            } else
            {
                charTimer = Time.time + charDelay;
            }

        }

        UpdateLines();

    }

    private void UpdateLines()
    {
        if (progressTracker >= selectedLine.Length)
        {
            lineFinished = true;
            progressTracker = 0;
            lineTimer = Time.time + lineDelay;
        }

        if (lineFinished && lineTimer < Time.time)
        {
            lineFinished = false;
            currentLine++;
            
            currentDisplay = "";
        }
        //print(lines.Length);
        if (currentLine >= lines.Length)
        {
            shouldSpeak = false;
        }
    }

    public void RequestItem()
    {
        lines[0] = "I want: " + storageScript.WantedObjectName;

        shouldSpeak = true;
    }

}
