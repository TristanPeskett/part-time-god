﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AnalyticsPing : MonoBehaviour
{

    public static int customersServed = 0;
    public static int itemsGrabbed = 0;
    public static int snakesCreated = 0;
    //public static float averageServeTime = 0f;

    //when customer is served
    //averageServeTime = (indivServeTime + averageServeTime) / customersServed;

    private void Awake()
    {
        SendPing();
    }

    public void SendPing()
    {
        //Analytics.CustomEvent("Ping");
    }

    private void OnApplicationQuit()
    {
        Debug.Log("Customers served: " + customersServed);
        Debug.Log("Items grabbed: " + itemsGrabbed);
        Debug.Log("Snakes Created: " + snakesCreated);
        Debug.Log("Average Serve Time: " + Time.time / customersServed);
        Debug.Log("Times Caught Using Powers: " + CustomerWantedList.PlayerCaughtCount);

        Analytics.CustomEvent("Customers Served", new Dictionary<string, object>
        {
            { "Number", customersServed }
        });
        Analytics.CustomEvent("Items Grabbed", new Dictionary<string, object>
        {
            { "Number", itemsGrabbed }
        });
        Analytics.CustomEvent("Snakes Created", new Dictionary<string, object> {

            { "Number", snakesCreated }
        });

        if (customersServed > 0)
        {
            Analytics.CustomEvent("Average Serve Time", new Dictionary<string, object>{

            { "Time", (Time.time / customersServed) }
        });
        }

        Analytics.CustomEvent("Times Caught Using Powers", new Dictionary<string, object>{

            { "Number", CustomerWantedList.PlayerCaughtCount }
        });

    }

    

}
