﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine.UI;

public class IsWanting : Action
{
    [SerializeField] Canvas MyCanvas;
    [SerializeField] Canvas YellowExclimationMark;
    [SerializeField] Canvas RedExclimationMark;
    [SerializeField] Animator anim;

    public bool isHappy = false;

    public override void OnStart()
    {
        MyCanvas = gameObject.transform.Find("Canvas").GetComponent<Canvas>();
        YellowExclimationMark = gameObject.transform.Find("YellowMarkCanvas").GetComponent<Canvas>();
        RedExclimationMark = gameObject.transform.Find("MarkCanvas").GetComponent<Canvas>();
        ///disable vision cone
        GetComponent<SphereCollider>().enabled = false;
        MyCanvas.enabled = true;
        YellowExclimationMark.enabled = false;
        RedExclimationMark.enabled = false;
        anim = transform.GetComponentInChildren<Animator>();

        ///get the global variable in
        CustomerWantedList.CurrentCustomerWhoWantsStuff.Add(gameObject);
        //change material
        //change these later when models are in
        //GetComponent<MeshRenderer>().material = CustomerWantedList.CustomerWantedList_NormalStateMaterial;
        //transform.Find("HisFace(Useless)").GetComponent<MeshRenderer>().material = CustomerWantedList.CustomerWantedList_NormalStateMaterial;

        GetComponent<DialogueSystem>().RequestItem();
    }


    //Ken's Movement Code
    ///rotate towards player
    public override TaskStatus OnUpdate()
    {
        if (GetComponent<GlobalCustomerStorage>().isHappy == false)
        {
            Vector3 targetDirection = CustomerWantedList.CustomerWantedList_PlayerTransform.position - transform.position;
            float singleStep = 5 * Time.deltaTime;
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDirection);
            anim.SetBool("Walking", false);
            return TaskStatus.Running;
        }
        else
            AnalyticsPing.customersServed++;
        anim.SetBool("Walking", true);
        return TaskStatus.Success;
        
    }
}
