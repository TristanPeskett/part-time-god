﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine.UI;

//This script is an Action script made specifically to be used with Behavior Designer. It is accessed through the behavior tree window and is run based on the trees logic. 
public class IsWandering : Action
{
    //Wander Location Variables
    [SerializeField]
    private GameObject[] allWanderLocations;
    [SerializeField]
    private GameObject[] chosenWanderLocations;
    [SerializeField]
    private int numberOfWanderLocations=4;
    [SerializeField] Animator anim;
    Rigidbody RB;

    //Navmesh Variables
    UnityEngine.AI.NavMeshAgent navMeshAgent;
    public float arrivalDistance=1;
    public float timeWaitingAtWanderLocation = 2;

    //Bool to determine if the customer is still wandering or if they have completed their wander action
    private bool customerIsWandering = true;

    public float visionConeDelay = 3;

    //Behavior Designer uses slightly different API to Monobehaviors so certain base methods (Like Awake and Start) require different syntax.
    public override void OnAwake()
    {
        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        anim = transform.GetComponentInChildren<Animator>();
        RB = GetComponent<Rigidbody>();


    }
    public override void OnStart()
    {
        customerIsWandering = true;
        FindWanderLocations();
        ChooseRandomWanderLocations();
        StartCoroutine(MoveToNextWanderLocation());

        StartCoroutine(WaitForVisionCone());
    }
 
    //This method tells the behavior tree whether this action has been completed or not
    public override TaskStatus OnUpdate()
    {
        if (customerIsWandering)
        {
            if (Vector3.Distance(transform.position, navMeshAgent.destination) <= 1.5f)
            {
                anim.SetBool("Walking", false);

            }
            return TaskStatus.Running;

        }
        else
            return TaskStatus.Success;
    }

    private void FindWanderLocations()
    {
        allWanderLocations = GameObject.FindGameObjectsWithTag("Wander");
    }

    //This method selects random locations for the customer to wander to out of the array of available locations found in the scene.
    private void ChooseRandomWanderLocations()
    {
        chosenWanderLocations = new GameObject[allWanderLocations.Length];
        for (int i = 0; i < numberOfWanderLocations; i++)
        {
            chosenWanderLocations[i] = allWanderLocations[Random.Range(0, allWanderLocations.Length)];
        }
    }

    //This coroutine goes through each random location and sends the customer there. They wait at the chosen location for a determined amount of seconds then move to the next one.
    //Once all locations have been visited, the bool customerIsWandering is set to false which tells the behavior tree this action has been completed.
    IEnumerator MoveToNextWanderLocation()
    {
        for (int i = 0; i < numberOfWanderLocations; i++)
        {
            navMeshAgent.SetDestination(chosenWanderLocations[i].transform.position);
            yield return new WaitUntil(HasArrivedAtWanderLocation);          
          

            yield return new WaitForSeconds(timeWaitingAtWanderLocation);
            anim.SetBool("Walking", true);
        }
        customerIsWandering = false;
    }

    //This delegate returns true once the customer has reached their current destination.
    public bool HasArrivedAtWanderLocation()
    {
        if (Vector3.Distance(transform.position, navMeshAgent.destination) <= arrivalDistance)
        {
           
            return true;

        }
        else
            return false; 


    }

    IEnumerator WaitForVisionCone()
    {
        yield return new WaitForSeconds(visionConeDelay);
        //change material
        //change these later when models are in
        //GetComponent<MeshRenderer>().material = CustomerWantedList.CustomerWantedList_DangerousStateMaterial;
        //transform.Find("HisFace(Useless)").GetComponent<MeshRenderer>().material = CustomerWantedList.CustomerWantedList_DangerousStateMaterial;
        ///enable vision cone
        GetComponent<SphereCollider>().enabled = true;
    }

    
}
