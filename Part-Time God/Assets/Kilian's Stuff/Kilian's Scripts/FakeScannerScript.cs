﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeScannerScript : MonoBehaviour
{
    [SerializeField]
    private GameObject gameManager;

    [SerializeField]
    private float itemValue;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            gameManager.GetComponent<GameManager>().ServedACustomer(itemValue);
        }
    }
}
