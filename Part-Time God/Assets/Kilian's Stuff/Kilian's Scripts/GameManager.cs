﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //UI Variables that need to be assigned in the inspector
    [SerializeField]
    private Text customersServedText = null;
    [SerializeField]
    private GameObject restartButton = null;
    [SerializeField]
    private Text loseText=null;
    [SerializeField]
    private Text winText = null;

    //Game objects that need to be assigned in the inspector
    [SerializeField]
    private GameObject mainCamera;
    [SerializeField]
    private GameObject player;

    //Served customer variables
    [SerializeField]
    private float gameWinScanThreshold=5;
    private float amountScanned = 0;

    [SerializeField]
    private int gameLossCaughtThreshold = 5;

    FMOD.Studio.Bus Bus;

    private void Start()
    {
        Bus = FMODUnity.RuntimeManager.GetBus("bus:/NonMusic");
        Bus.setMute(true);
    }
    private void Update()
    {
        if (CustomerWantedList.PlayerCaughtCount >= gameLossCaughtThreshold)
        {
            StartCoroutine(LoseGame());
        }
        //Update the customers served UI text as the number changes
        customersServedText.text = "$" + amountScanned;
        CustomerCheck();
    }

    public void UnMuteSoundEffects()
    {
        Bus.setMute(false);
    }

    //Public function that can be called to run the GameStart coroutine
    public void CallGameStartCoroutine()
    {
        StartCoroutine(GameStart());
    }

    //Coroutine that runs when the game starts
    private IEnumerator GameStart()
    {
        customersServedText.gameObject.SetActive(true);
        yield return null;
    }

    //Public function that can be called by other scripts to increase the customerServed value
    public void ServedACustomer(float itemValue)
    {
        amountScanned+=itemValue;
    }

    //Function that checks every update for the win state threshold
    private void CustomerCheck()
    {
        if (amountScanned >= gameWinScanThreshold)
        {
            StartCoroutine(WinGame());
        }
    }

    //Function that is called when the restart button is clicked to restart the game
    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }

    //Lose and win state coroutines that run all functions required during a lose or win state.
    private IEnumerator LoseGame()
    {
        loseText.gameObject.SetActive(true);
        restartButton.SetActive(true);
        mainCamera.GetComponent<CameraView>().enabled = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        player.GetComponent<FirstPersonMovement>().enabled = false;
        yield return null;
    }

    private IEnumerator WinGame()
    {
        winText.gameObject.SetActive(true);
        restartButton.SetActive(true);
        mainCamera.GetComponent<CameraView>().enabled = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        player.GetComponent<FirstPersonMovement>().enabled = false;
        yield return null;
    }

    //Public methods that can be called by other scripts to start the win or lose game coroutine
    public void GameLoss()
    {
        StartCoroutine(LoseGame());
    }

    public void GameWin()
    {
        StartCoroutine(WinGame());
    }
}
