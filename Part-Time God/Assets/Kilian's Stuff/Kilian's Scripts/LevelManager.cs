﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public Animator transition;
    [SerializeField]
    private float transitionTime = 2;

    public void CallLoadNextLevelCoroutine()
    {
        StartCoroutine(LoadNextLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }
    IEnumerator LoadNextLevel(int levelIndex)
    {
        
        transition.SetTrigger("LevelEnd");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(levelIndex);
    }
    public void CallRestartLevelCoroutine()
    {
        StartCoroutine(RestartLevel());
    }
    public IEnumerator RestartLevel()
    {
        transition.SetTrigger("LevelEnd");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
