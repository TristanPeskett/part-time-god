﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine.UI;

public class IsLeaving : Action
{
    [SerializeField]
    private GameObject leavingLocation;

    //Navmesh Variables
    UnityEngine.AI.NavMeshAgent navMeshAgent;
    public float arrivalDistance = 1;

    private bool customerIsLeaving = true;

    [SerializeField] Text MyText;
    public override void OnAwake()
    {
        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        MyText = gameObject.transform.Find("Canvas").transform.Find("PanelBack").transform.Find("PanelInner").transform.Find("Text").GetComponent<Text>();
    }
    public override void OnStart()
    {
        customerIsLeaving = true;
        leavingLocation = GameObject.FindGameObjectWithTag("LeavingLocation");
        ///remove object from global wantting array
        CustomerWantedList.CurrentCustomerWhoWantsStuff.Remove(gameObject);
        MyText.text = "I am happy";
    }
    public override TaskStatus OnUpdate()
    {
        Leave();
        if (HasArrivedAtLeavingLocation())
        {
            GetComponent<GlobalCustomerStorage>().destroyGameObject = true;
            return TaskStatus.Success;
        }
        else return TaskStatus.Running;
    }

    private void Leave()
    {
        navMeshAgent.SetDestination(leavingLocation.transform.position);
    }

    public bool HasArrivedAtLeavingLocation()
    {
        if (Vector3.Distance(transform.position, navMeshAgent.destination) <= arrivalDistance)
            return true;
        else
            return false;
    }
}
