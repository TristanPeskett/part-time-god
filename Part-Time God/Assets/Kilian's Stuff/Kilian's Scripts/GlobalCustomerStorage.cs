﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class GlobalCustomerStorage : MonoBehaviour
{
    public bool destroyGameObject = false;

    public string WantedObjectName;

    public bool isHappy = false;

    ///these are for when customer sees player use ability
    public bool CaughtPlayerUsingAbility = false;
    [SerializeField] Canvas MyCaughtPlayerMarkCanvas;
    public bool CanPlayerBeCaught = true;
    float CaughtWarningDelay = 2;
    float CaughtWarningTimer;
    float CatchingDelay = 5;
    float MarkAppearTimer = 3;

    [FMODUnity.EventRef]
    public string CustomerAlertEvent = "";

    private void Start()
    {
        MyCaughtPlayerMarkCanvas = gameObject.transform.Find("MarkCanvas").GetComponent<Canvas>();
        MyCaughtPlayerMarkCanvas.enabled = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (destroyGameObject)
        {
            CustomerWantedList.customersInScene.Remove(gameObject);
            Destroy(this.gameObject);
        }
        if (CaughtPlayerUsingAbility == true && CanPlayerBeCaught == true)
        {
            StartCoroutine(playerCaught());
            StartCoroutine(playerCaughtMarkAppear());
        }
    }

    ///when caught player using ability
    IEnumerator playerCaught()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(CustomerAlertEvent, this.gameObject);
        CanPlayerBeCaught = false;
        //change this for game manager global variable
        CustomerWantedList.PlayerCaughtCount += 1;
        yield return new WaitForSeconds(CatchingDelay);
        CaughtPlayerUsingAbility = false;
        CanPlayerBeCaught = true;
    }
    IEnumerator playerCaughtMarkAppear()
    {
        MyCaughtPlayerMarkCanvas.enabled = true;
        yield return new WaitForSeconds(MarkAppearTimer);
        MyCaughtPlayerMarkCanvas.enabled = false;
        
    }




}
