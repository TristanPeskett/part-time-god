﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine.UI;

public class IsSpawning : Action
{
    ///customer canvas related
    [SerializeField] Canvas MyCanvas;
    [SerializeField] Text MyText;
    public string WanttedObjectName;
    ///these are for when customer sees player use ability
    public bool CaughtPlayerUsingAbility = false;
    [SerializeField] Canvas MyCaughtPlayerMarkCanvas;
    bool CanPlayerBeCaught = true;
    float CatchingDelay = 5;
    float MarkAppearTimer = 3;
    public override void OnAwake()
    {
        ///disable vision cone on start
        GetComponent<SphereCollider>().enabled = false;
    }

    public override void OnStart()
    {
        ///find canvas objects, disable canvas on start, random a object for the customer's wanted object
        MyCanvas = gameObject.transform.Find("Canvas").GetComponent<Canvas>();
        MyCaughtPlayerMarkCanvas = gameObject.transform.Find("MarkCanvas").GetComponent<Canvas>();
        MyText = gameObject.transform.Find("Canvas").transform.Find("PanelBack").transform.Find("PanelInner").transform.Find("Text").GetComponent<Text>();
        MyCanvas.enabled = false;
        MyCaughtPlayerMarkCanvas.enabled = false;
        int Rand = Random.Range(0, (CustomerWantedList.CustomerWantedList_HowManyObjects - 1));
        WanttedObjectName = CustomerWantedList.CustomerWantedList_Name[Rand];
        MyText.text = "I want: " + WanttedObjectName;
        GetComponent<GlobalCustomerStorage>().WantedObjectName = WanttedObjectName;
    }

    public override TaskStatus OnUpdate()
    {
        return TaskStatus.Success;
    }
}
