﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IsWalkingToCounter : Action
{
    [SerializeField]
    private GameObject counterLocation;

    //Navmesh Variables
    UnityEngine.AI.NavMeshAgent navMeshAgent;
    public float arrivalDistance = 1;

    private bool customerIsWalkingToCounter = true;
    public override void OnAwake()
    {
        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }
    public override void OnStart()
    {
        customerIsWalkingToCounter = true;
        counterLocation = GameObject.FindGameObjectWithTag("Counter");
    }
    public override TaskStatus OnUpdate()
    {
        MoveToCounter();
        if (HasArrivedAtCounter())
        {
            return TaskStatus.Success;
        }
        else return TaskStatus.Running;
    }

    private void MoveToCounter()
    {
        navMeshAgent.SetDestination(counterLocation.transform.position);
    }

    public bool HasArrivedAtCounter()
    {
        if (Vector3.Distance(transform.position, navMeshAgent.destination) <= arrivalDistance)
            return true;
        else
            return false;
    }




}
