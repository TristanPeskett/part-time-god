﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scanner_Ken : MonoBehaviour
{
    [SerializeField] private GameObject gameManager;
    //do all scan shiet here
    private void OnTriggerEnter(Collider other)
    {
        ///if that object is not a scannable
        if (other.gameObject.GetComponent<Scannable>() == null)
        {
            // play a sound here?
            Debug.Log("Can't Scan Kinky Stuff");       
            return;
        }

        ///if that object is a scannable
        if (other.gameObject.GetComponent<Scannable>() != null && CustomerWantedList.CurrentCustomerWhoWantsStuff.Count > 0)
        {
            Debug.Log("scanned a thing");
                CustomerWantedList.LatestScannedObject = other.gameObject;
        }
        
    }
}
