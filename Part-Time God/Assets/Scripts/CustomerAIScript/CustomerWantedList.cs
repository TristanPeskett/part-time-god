﻿using UnityEngine.Audio;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class CustomerWantedList : MonoBehaviour
{
    bool GameStarted = false;

    ///this is a class which stores all the scannable object informations
    [SerializeField] int HowManyObjects;
    public static int CustomerWantedList_HowManyObjects;
    [SerializeField] string[] Name;
    public static List<string> CustomerWantedList_Name = new List<string>();

    ///these are for making sure right thing is scanned for the right customer
    public static List<GameObject> CurrentCustomerWhoWantsStuff = new List<GameObject>();
    public static GameObject LatestScannedObject;
    [SerializeField] private GameObject gameManager;

    ///these are for instantiating the customers
    [SerializeField] float InstantiateDelay;
    float InstantiateDelayTimer;
    [SerializeField] Transform InstantiateOnThisTransfrom;
    public static Transform CustomerWantedList_CustomerInstantiateLocation;
    [SerializeField] List<Transform> CustomerPrefab = new List<Transform>();

    /// these are for recording locations
    [SerializeField] Transform CustomerCheckOutLocation;
    public static Transform CustomerWantedList_CustomerCheckOutLocation;
    [SerializeField] Transform CustomerEnterStoreLocation;
    public static Transform CustomerWantedList_CustomerEnterStoreLocation;
    [SerializeField] Transform PlayerTransform;
    public static Transform CustomerWantedList_PlayerTransform;

    //temp materials for customer color changes between dangerous state vs normal state
    [SerializeField] Material NormalStateMaterial;
    public static Material CustomerWantedList_NormalStateMaterial;
    [SerializeField] Material DangerousStateMaterial;
    public static Material CustomerWantedList_DangerousStateMaterial;

    [SerializeField] Transform PlayerTransfrom;

    //temp global int for how many times player got caught using ability
    public static int PlayerCaughtCount;
    ///for UI
    [SerializeField] Text PlayerCaughtCountText;

    [SerializeField] int numberOfCustomersAllowed;
    public static List<GameObject> customersInScene = new List<GameObject>();

    void Awake()
    {
        ///assign the informations to static variables
        CustomerWantedList_HowManyObjects = HowManyObjects;
        foreach(string name in Name)
        {
            CustomerWantedList_Name.Add(name);
        }
        CustomerWantedList_CustomerInstantiateLocation = InstantiateOnThisTransfrom;
        InstantiateDelayTimer = InstantiateDelay;
        CustomerWantedList_CustomerCheckOutLocation = CustomerCheckOutLocation;
        CustomerWantedList_CustomerEnterStoreLocation = CustomerEnterStoreLocation;
        CustomerWantedList_PlayerTransform = PlayerTransform;
        CustomerWantedList_NormalStateMaterial = NormalStateMaterial;
        CustomerWantedList_DangerousStateMaterial = DangerousStateMaterial;
    }

    private void Update()
    {
        if (GameStarted == true)
        {
            ///instnaite customer with delay
            if (InstantiateDelayTimer > 0)
            {
                InstantiateDelayTimer -= Time.deltaTime;
            }
            if (InstantiateDelayTimer <= 0 && customersInScene.Count < numberOfCustomersAllowed)
            {
                InstantiateDelayTimer = InstantiateDelay;
                Transform Customer;
                int i = (Random.Range(0, CustomerPrefab.Count));
                Customer = Instantiate(CustomerPrefab[i], CustomerWantedList_CustomerInstantiateLocation.position, Quaternion.identity);
                //Customer.GetComponent<CustomerVisionCone>().target = PlayerTransfrom;
                Customer.SetParent(null);
                customersInScene.Add(Customer.gameObject);
            }
        }


        ///if anything is scanned, check if it fits the wantted list
        if (LatestScannedObject != null)
        {
            ///if the customer actually wants this item
            foreach (GameObject G in CurrentCustomerWhoWantsStuff)
            {
                if (G != null)
                {
                    if (LatestScannedObject.GetComponent<Scannable>().Name == G.GetComponent<GlobalCustomerStorage>().WantedObjectName)
                    {
                        ///change customer states to happy
                        G.GetComponent<GlobalCustomerStorage>().isHappy = true;
                        ///send in the earned amount to the global variable
                        gameManager.GetComponent<GameManager>().ServedACustomer(LatestScannedObject.gameObject.GetComponent<Scannable>().HowMuchDollar);
                        Destroy(LatestScannedObject);
                        LatestScannedObject = null;
                        return;

                    }
                }
            }
           LatestScannedObject = null;
        }

        ///caught count ui
        //change variable to game controller global variabel
        PlayerCaughtCountText.text = "" + PlayerCaughtCount;
    }

    public void StartGameForCustomerToInstantiate()
    {
        GameStarted = true;
    }
}