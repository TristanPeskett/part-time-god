﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerInStoreCheck : MonoBehaviour
{
    ///This is a trigger box to end customer's entering store phase
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Customer")
        {
            collision.gameObject.GetComponent<CustomerBehavior>().isWalkingIntoStore = false;
        }
    }
}
