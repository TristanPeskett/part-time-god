﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerVisionCone : MonoBehaviour
{
    ///simple vision count
    public Transform target;

    [SerializeField] float ConeAngle;
    void Update()
    {
        Vector3 targetDir = target.position - transform.position;
        float angle = Vector3.Angle(targetDir, transform.forward);

        ///when player is in vision cone, got caught count goes up
        //change to only fires when player is dragging stuff around (global variable in dragging script, turn it on and off and check it here)
        if (angle < ConeAngle && FirstPersonMovement.usingDragging) {

            GetComponent<GlobalCustomerStorage>().CaughtPlayerUsingAbility = true;

            //AnalyticsPing.timesCaught++;
        }
    }
}
