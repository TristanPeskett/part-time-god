﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionCone_WarningOnly : MonoBehaviour
{
    [SerializeField]
    private float visionAngle = 30;

    [SerializeField] GameObject CaughtVisionConeObject;
    [SerializeField] GameObject MyExclaimationMark;

    bool StartTimer = false;
    float WarningTimer = 0.5f;
    float WarningTimerCount = 0;
    float anotherTimer = 1;
    bool once = true;

    // Start is called before the first frame update
    void Start()
    {
        CaughtVisionConeObject.GetComponent<Collider>().enabled = false;
        MyExclaimationMark.SetActive(false);
    }

    private void Update()
    {
        if(StartTimer == true)
        {
            WarningTimerCount += Time.deltaTime;
        }
        if(StartTimer == false)
        {
            WarningTimerCount = 0;
        }
        if(GetComponent<Collider>().enabled == false)
        {
            StartTimer = false;
        }
        if(GetComponent<GlobalCustomerStorage>().CanPlayerBeCaught == false)
        {
            StartTimer = false;
            MyExclaimationMark.SetActive(false);
        }


        if(WarningTimerCount >= WarningTimer)
        {
            WarningTimerCount = 0;
            CaughtVisionConeObject.GetComponent<Collider>().enabled = true;
            MyExclaimationMark.SetActive(false);
        }

        
        if (CaughtVisionConeObject.GetComponent<Collider>().enabled == true)
        {
            anotherTimer -= Time.deltaTime;
            if(anotherTimer <= 0)
            {
                anotherTimer = 1;
                CaughtVisionConeObject.GetComponent<Collider>().enabled = false;
                StartTimer = false;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //If the object isn't draggable, stop
        if (!other.gameObject.GetComponent<DragRigidbody>())
            return;

        //If the object is floating
        if (other.gameObject.GetComponent<DragRigidbody>().floating)
        {
            //Determine vision angle, if within range get caught
            Vector3 targetDir = other.gameObject.transform.position - transform.position;
            float angle = Vector3.Angle(targetDir, transform.forward);
            if (angle < visionAngle)
            {
                StartTimer = true;
                MyExclaimationMark.SetActive(true);
            }
            else
            {
                StartTimer = false;
                MyExclaimationMark.SetActive(false);
            }
            //Debug.Log(angle);

            //Loss

        }
    }
}
