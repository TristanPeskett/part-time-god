﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerBehavior : MonoBehaviour
{
    ///customer states
    public bool isWalkingIntoStore;
    public bool isWondering;
    public bool isWantting;
    public bool isHappy;
    ///customer canvas related
    [SerializeField] Canvas MyCanvas;
    [SerializeField] Text MyText;
    public string WanttedObjectName;
    ///these are for when customer sees player use ability
    public bool CaughtPlayerUsingAbility = false;
    [SerializeField] Canvas MyCaughtPlayerMarkCanvas;
    bool CanPlayerBeCaught = true;
    float CatchingDelay = 5;
    float MarkAppearTimer = 3;

    //remove
    float t;

    private void Awake()
    {
        ///disable vision cone on start
        GetComponent<CustomerVisionCone>().enabled = false;
    }

    void Start()
    {
        ///find canvas objects, disable canvas on start, random a object for the customer's wanted object
        MyCanvas = gameObject.transform.Find("Canvas").GetComponent<Canvas>();
        MyCaughtPlayerMarkCanvas = gameObject.transform.Find("MarkCanvas").GetComponent<Canvas>();
        MyText = gameObject.transform.Find("Canvas").transform.Find("PanelBack").transform.Find("PanelInner").transform.Find("Text").GetComponent<Text>();
        MyCanvas.enabled = false;
        MyCaughtPlayerMarkCanvas.enabled = false;
        int Rand = Random.Range(0, (CustomerWantedList.CustomerWantedList_HowManyObjects - 1));
        WanttedObjectName = CustomerWantedList.CustomerWantedList_Name[Rand];
        MyText.text = "I want: " + WanttedObjectName;

        ///set starting states
        isWalkingIntoStore = true;
        isWondering = false;
        isWantting = false;
        isHappy = false;

        //remove
        t = 2;
    }

    void Update()
    {
        ///if customer is walking into store, it is moving toward the location
        if(isWalkingIntoStore == true)
        {
            ///move there
            transform.position = Vector3.MoveTowards(transform.position, CustomerWantedList.CustomerWantedList_CustomerEnterStoreLocation.position, 0.03f);
            ///rotate towards there
            Vector3 targetDirection = CustomerWantedList.CustomerWantedList_CustomerEnterStoreLocation.position - transform.position;
            float singleStep = 5 * Time.deltaTime;
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDirection);
        }

        ///if customer is no longer walking into store, it is now wondering
        if(isWalkingIntoStore == false && isWondering == false && isWantting == false && isHappy == false)
        {
            isWondering = true;
            ///enable vision cone
            GetComponent<CustomerVisionCone>().enabled = true;
        }

        //do wondering stuff here, for now null
        if(isWondering == true)
        {
            //change material
            //change these later when models are in
            GetComponent<MeshRenderer>().material = CustomerWantedList.CustomerWantedList_DangerousStateMaterial;
            transform.Find("HisFace(Useless)").GetComponent<MeshRenderer>().material = CustomerWantedList.CustomerWantedList_DangerousStateMaterial;
            //wondering here
            // change these
            if(t > 0) { t -= Time.deltaTime; }
            if(t <= 0) { isWondering = false; isWantting = true; }
        }

        ///state controller
        if(isWalkingIntoStore == false && isWondering == false && isWantting == false && isHappy == false)
        {
            isWantting = true;
        }

        ///if customer is wantting stuff, show the canvas
        if (isWantting == true && MyCanvas.enabled == false)
        {
            ///disable vision cone
            GetComponent<CustomerVisionCone>().enabled = false;
            MyCanvas.enabled = true;
            ///get the global variable in
            CustomerWantedList.CurrentCustomerWhoWantsStuff.Add(gameObject);
            //change material
            //change these later when models are in
            GetComponent<MeshRenderer>().material = CustomerWantedList.CustomerWantedList_NormalStateMaterial;
            transform.Find("HisFace(Useless)").GetComponent<MeshRenderer>().material = CustomerWantedList.CustomerWantedList_NormalStateMaterial;
        }
        if (isWantting == true)
        {
            ///move there
            transform.position = Vector3.MoveTowards(transform.position, CustomerWantedList.CustomerWantedList_CustomerCheckOutLocation.position, 0.03f);
            ///rotate towards player
            Vector3 targetDirection = CustomerWantedList.CustomerWantedList_PlayerTransform.position - transform.position;
            float singleStep = 5 * Time.deltaTime;
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDirection);
        }

        ///if customer got wantted stuff scanned, he is happy, and does stuff
        if (isHappy == true)
        {
            ///remove object from global wantting array
            CustomerWantedList.CurrentCustomerWhoWantsStuff.Remove(gameObject);
            isWantting = false;
            MyText.text = "I am happy";

            /// customer will now walk out of the store and get destroyed
            ///move there
            transform.position = Vector3.MoveTowards(transform.position, CustomerWantedList.CustomerWantedList_CustomerInstantiateLocation.position, 0.03f);
            ///rotate towards location
            Vector3 targetDirection = CustomerWantedList.CustomerWantedList_PlayerTransform.position - transform.position;
            float singleStep = 5 * Time.deltaTime;
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDirection);

            if (transform.position == CustomerWantedList.CustomerWantedList_CustomerInstantiateLocation.position)
            {
                Destroy(gameObject);
            }
        }

        ///when player is caught using ability
        if(CaughtPlayerUsingAbility == true && CanPlayerBeCaught == true)
        {
            StartCoroutine(playerCaught());
            StartCoroutine(playerCaughtMarkAppear());
        }
    }

    ///when caught player using ability
    IEnumerator playerCaught()
    {
        CanPlayerBeCaught = false;
        //change this for game manager global variable
        CustomerWantedList.PlayerCaughtCount += 1;
        yield return new WaitForSeconds(CatchingDelay);
        CaughtPlayerUsingAbility = false;
        CanPlayerBeCaught = true;
    }
    IEnumerator playerCaughtMarkAppear()
    {
        MyCaughtPlayerMarkCanvas.enabled = true;
        yield return new WaitForSeconds(MarkAppearTimer);
        MyCaughtPlayerMarkCanvas.enabled = false;
    }
}
