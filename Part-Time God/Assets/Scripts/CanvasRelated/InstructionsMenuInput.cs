﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionsMenuInput : MonoBehaviour
{
    int States = 0;
    float delay = 0f;

    bool GameStarted = false;
    void Update()
    {
        if (GameStarted)
        {
            if (delay > 0f)
            {
                delay -= Time.deltaTime;
            }

            ///when game just started, player press the m key for the first time, play note fade in anim
            if (Input.GetKeyDown(KeyCode.M) && States == 0 && delay <= 0)
            {
                States = 1;
                delay = 0.8f;
                GetComponent<Animator>().SetBool("FadeIn", true);
                GetComponent<Animator>().SetBool("FadeOut", false);
            }

            ///when note is on screen, player press m again to turn it off
            if (Input.GetKeyDown(KeyCode.M) && States == 1 && delay <= 0)
            {
                States = 0;
                delay = 0.8f;
                GetComponent<Animator>().SetBool("FadeOut", true);
                GetComponent<Animator>().SetBool("FadeIn", false);
            }
        }
    }

    public void StartGameForInstructionMenu()
    {
        GameStarted = true;
    }
}
