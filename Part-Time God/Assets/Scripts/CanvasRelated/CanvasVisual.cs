﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CanvasVisual : MonoBehaviour
{
    [SerializeField] Image StartButtonImage;
    [SerializeField] Sprite ButtonOff;
    [SerializeField] Sprite ButtonOn;
    [SerializeField] Text GotIt;
    [SerializeField] Text GO;
    [SerializeField] GameObject StartBackground;
    void Start()
    {
        StartButtonImage.sprite = ButtonOff;
        GO.enabled = false;
    }

    void Update()
    {
        if(StartBackground.activeSelf == false && StartButtonImage.gameObject.activeSelf == true)
        {
            StartBackground.SetActive(true);
        }

        if (StartBackground.activeSelf == true && StartButtonImage.gameObject.activeSelf == false)
        {
            StartBackground.SetActive(false);
        }
    }

    public void onHoverImageChange()
    {
        StartButtonImage.sprite = ButtonOn;
        GO.enabled = true;
        GotIt.enabled = false;
    }

    public void offHoverImageChange()
    {
        StartButtonImage.sprite = ButtonOff;
        GO.enabled = false;
        GotIt.enabled = true;
    }

    public void ResetCaughtCount()
    {

        CustomerWantedList.PlayerCaughtCount = 0;
    }
}
