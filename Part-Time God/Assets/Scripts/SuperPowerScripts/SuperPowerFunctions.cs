﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperPowerFunctions : MonoBehaviour
{
    // record power corresponding public static numbers here (SuperPowerSelector - CurrentSelecttedPower)
    // 1 - Snake Touch (turn objects into snakes on mouse press)
    // 2 - Gravity Suck (suck objects to player direction on mouse hold)

    ///prefab for snake, so we can turn things into snakes
    [SerializeField] Transform SnakePrefab;

    private void Awake()
    {

    }

    public void SnakeTouch(Transform SelecttedObject)
    {
        //might need to instantiate the snake on top of object instead of "in" the object

        ///See what object player used power on
        ///if it is something that can be turned into snakes
        //change these tags later if needed
        if (SelecttedObject.tag != "Wall" && SelecttedObject.tag != "Snake" && SelecttedObject.tag != "Player")
        {
            //add special affects and shiets here
            ///debug what got turned into snake
            Debug.Log("Turned ( " + SelecttedObject.name + " ) into snake!");
            ///instantiate snake on selected object
            Instantiate(SnakePrefab, SelecttedObject.position, Quaternion.identity);

            AnalyticsPing.snakesCreated++;

            if (SelecttedObject.GetComponent<GlobalCustomerStorage>() != null)
            {
                CustomerWantedList.LatestScannedObject = null;
                CustomerWantedList.customersInScene.Remove(SelecttedObject.gameObject);
                CustomerWantedList.CurrentCustomerWhoWantsStuff.Remove(SelecttedObject.gameObject);
                Debug.Log("removed " + SelecttedObject);
            }
            ///destroy that object (can be turned into object pooling if we really want to)
            Destroy(SelecttedObject.gameObject);
        }

        ///if it is not something
        if(SelecttedObject.tag == "Wall" || SelecttedObject.tag == "Snake")
        {
            //maybe add something else here for more juice later?
            ///debug we can't turn that into snake
            Debug.Log("Cannot turn a ( " + SelecttedObject.name + " ) into snake!");
        }
    }

    //this is just an example
    public void ExampleSuperPower(Transform SelecttedObject)
    {
        Debug.Log("Example super power used :3");
    }

    //these updates are for functions which might need to be held to use
    private void Update()
    {
        ///if player is currently holding key for power
        if(Input_SuperPower.CurrentlyHoldingThePowerUpKey == true)
        {

        }
        ///if player released the key
        if (Input_SuperPower.CurrentlyHoldingThePowerUpKey == false)
        {

        }
    }
}
