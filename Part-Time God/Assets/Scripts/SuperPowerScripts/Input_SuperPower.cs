﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Input_SuperPower : MonoBehaviour
{
    ///this bool is for powers which might need holding
    public static bool CurrentlyHoldingThePowerUpKey = false;

    private void Awake()
    {
        CurrentlyHoldingThePowerUpKey = false;
    }
    void Update()
    {
        //for now
        //can be changed into other key bindings if left mouse button needs to be used for something else
        ///on left mouse button down, set the raycast bool to true
        if (Input.GetKeyDown(KeyCode.F))
        {
            RayCastForwardCamera_Ken.SendRaycastInfrontOfCamera_Ken = true;
            CurrentlyHoldingThePowerUpKey = true;
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            CurrentlyHoldingThePowerUpKey = false;
        }
    }
}
