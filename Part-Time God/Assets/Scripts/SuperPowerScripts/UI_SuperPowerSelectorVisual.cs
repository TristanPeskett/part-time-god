﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UI_SuperPowerSelectorVisual : MonoBehaviour
{
    public static bool SuperPowerChanged_UI = false;
    [SerializeField] GameObject SnakeTouch;
    [SerializeField] GameObject Empty;
    void Start()
    {
        SuperPowerChanged_UI = false;
        //set UIs to false, will change these to something else later (better looking)
        SnakeTouch.SetActive(true);
        Empty.SetActive(false);
    }

    //disable all the UIs for once
    void DisableAllPowerUisForOnce()
    {
        SnakeTouch.SetActive(false);
        Empty.SetActive(false);
    }
    void Update()
    {
        //update the ui based on current selectted super power
        if (SuperPowerChanged_UI == true)
        {
            SuperPowerChanged_UI = false;
            //keep adding
            if(SuperPowerSelector.CurrentSelecttedPower == 1)
            {
                DisableAllPowerUisForOnce();
                SnakeTouch.SetActive(true);
            }
            if (SuperPowerSelector.CurrentSelecttedPower == 2)
            {
                DisableAllPowerUisForOnce();
                Empty.SetActive(true);
            }
        }
    }
}
