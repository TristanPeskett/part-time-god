﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperPowerSelector : MonoBehaviour
{
    [SerializeField] int MaxNumberOfPowers;
    //record all powers and corresponding numbers
    // currently has 3
    public static int CurrentSelecttedPower;

    ///just in case if the mouse wheel need any delay
    float ChangePowerDelay;
    [SerializeField] float ChangePowerDelayNumber;

    void Start()
    {
        ///set power to the first power on start
        CurrentSelecttedPower = 1;
    }

    void Update()
    {
        //these are for scrolling mouse change power, still needs to be modified later
        /// mouse wheel scroll delay
        if(ChangePowerDelay > 0)
        {
            ChangePowerDelay -= Time.deltaTime;
        }

        ///when player is not currently holding down the power key, they can scroll mouse wheel to change powers
        if (Input_SuperPower.CurrentlyHoldingThePowerUpKey == false)
        {
            if ((Input.GetKeyDown(KeyCode.E) && ChangePowerDelay <= 0))
            {
                ChangePowerDelay = ChangePowerDelayNumber;

                ///this bool is a buffer to not run the second part of the code if the first is run
                bool selected = false;
                ///when scroll up, and selected new power = false
                ///set selected new power to true, so it won't fire another function, and change the power number
                if (CurrentSelecttedPower != MaxNumberOfPowers && selected == false)
                {
                    selected = true;
                    ///this bool is used for updating UI visual for once
                    UI_SuperPowerSelectorVisual.SuperPowerChanged_UI = true;
                    CurrentSelecttedPower += 1;
                }
                ///start from beginning
                if (CurrentSelecttedPower == MaxNumberOfPowers && selected == false)
                {
                    selected = true;
                    UI_SuperPowerSelectorVisual.SuperPowerChanged_UI = true;
                    CurrentSelecttedPower = 1;
                }
            }
            if ((Input.GetKeyDown(KeyCode.Q) && ChangePowerDelay <= 0))
            {
                ChangePowerDelay = ChangePowerDelayNumber;

                ///this bool is a buffer to not run the second part of the code if the first is run
                bool selected = false;
                if (CurrentSelecttedPower != 1 && selected == false)
                {
                    selected = true;
                    UI_SuperPowerSelectorVisual.SuperPowerChanged_UI = true;
                    CurrentSelecttedPower -= 1;
                }
                ///start from end
                if (CurrentSelecttedPower == 1 && selected == false)
                {
                    selected = true;
                    UI_SuperPowerSelectorVisual.SuperPowerChanged_UI = true;
                    CurrentSelecttedPower = MaxNumberOfPowers;
                }
            }
        }
    }
}
