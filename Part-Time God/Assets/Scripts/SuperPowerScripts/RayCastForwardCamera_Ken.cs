﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastForwardCamera_Ken : MonoBehaviour
{
    ///a public static bool, when it = true, will send out a raycast to the middle of the screen. Which will fire selectted super power on that thing hit
    public static bool SendRaycastInfrontOfCamera_Ken;
    public Camera MainCamera;
    SuperPowerFunctions SuperPowerFunctionsScript;
    void Awake()
    {
        ///set the static bool to false on awake
        SendRaycastInfrontOfCamera_Ken = false;
        //*find the SuperPowerFunctionsScript on this gameObject (needs to be on the same gameobject)
        SuperPowerFunctionsScript = gameObject.GetComponent<SuperPowerFunctions>();
    }

    void Update()
    {
        ///when the ratcast bool is set to true, set to back to false(so it only fire once), and start the raycastting and super power functions
        if (SendRaycastInfrontOfCamera_Ken == true)
        {
            SendRaycastInfrontOfCamera_Ken = false;
            RaycastInfrontOfCamera();
        }
    }

    ///simple raycast function which returns hit object as a transform
    void RaycastInfrontOfCamera()
    {
        RaycastHit hit;
        Ray ray = MainCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;

            //check which power is selectted(from SuperPowerSelector script), and use that power function (from SuperPowerFunctions)
            SuperPowerFunctionsScript.SnakeTouch(objectHit);
        }
        else
        {
            Debug.Log("Raycast did not hit anything");
        }
    }
}
