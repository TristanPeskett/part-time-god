﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraView : MonoBehaviour
{
    public float mouseSensitivity = 1000.0f;
    public float clampAngle = 90.0f;

    private Transform firstPersonBody;
    private float rotX = 0.0f;

    public bool gameStarted = false;

    void Start()
    {
        //transform.root finds the transform of the topmost parent in this objects hierarchy
        firstPersonBody = transform.root;
    }

    void Update()
    {
            Cursor.visible = false;
            //Lock the cursor to the center of the screen
            Cursor.lockState = CursorLockMode.Locked;
            //Gets mouse input and multiplies it by controllable sensitivity and delta time to keep in check with framerate
            float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

            //Mouse y rotates just the camera inversely to the x axis
            rotX -= mouseY;
            //Clamp the rotation angle so the player can't look further than straight up or straight down
            rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

            //Mouse x rotates the entire rigidbody of the firstPersonBody around the y axis(Vector3.up)
            transform.localRotation = Quaternion.Euler(rotX, 0f, 0f);
            firstPersonBody.Rotate(Vector3.up * mouseX);
    }
}
