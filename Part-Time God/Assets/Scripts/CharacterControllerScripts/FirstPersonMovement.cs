﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script must be attached to the player object. 
[RequireComponent(typeof(CharacterController))]
public class FirstPersonMovement : MonoBehaviour
{
    private CharacterController controller;

    public float moveSpeed = 10;

    public static bool usingDragging = false;
    float x ;
    float z ;

    private void Awake()
    {
        controller = GetComponent<CharacterController>();
        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");
    }

    private void Update()
    {
            

            Vector3 move = transform.right * x + transform.forward * z;

            controller.Move(move * moveSpeed * Time.deltaTime);
    }
}
